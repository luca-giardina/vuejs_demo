import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

import Dashboard from '@/components/Dashboard/Dashboard'
import Country from '@/components/Country'
import VueCharts from 'vue-chartjs'

Vue.use(BootstrapVue)
Vue.use(VueCharts)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Dashboard
    }, {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    }, {
      path: '/country/:contryid',
      name: 'Country',
      component: Country
    }
  ]
})
